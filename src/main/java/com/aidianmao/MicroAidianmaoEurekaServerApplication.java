package com.aidianmao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MicroAidianmaoEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroAidianmaoEurekaServerApplication.class, args);
	}
}
